# Wake up docker containers
up:
	docker-compose up -d

# Shut down docker containers
down:
	docker-compose down

# Show a status of each container
status:
	docker-compose ps

# Status alias
s: status

# Show logs of each container
logs:
	docker-compose logs

# Restart all containers
restart: down up

# Build and up docker containers
build:
	docker-compose up -d --build

# Build containers with no cache option
build-no-cache:
	docker-compose build --no-cache

# Build and up docker containers
rebuild: down build


#-----------------------------------------------------------
# Database
#-----------------------------------------------------------

# Dump database into file
db-dump:
	docker-compose exec db pg_dump -U app -d app > dumps/dump.sql


#-----------------------------------------------------------
# Dependencies
#-----------------------------------------------------------

# Install composer dependencies
composer-install:
	docker-compose exec php composer install

# Update composer dependencies
composer-update:
	docker-compose exec php composer update

# Update all dependencies
dependencies-update: composer-update

#-----------------------------------------------------------
# Installation
#-----------------------------------------------------------

env:
	cp .env.example .env

# Add permissions for Laravel cache and storage folders
permissions:
	sudo chmod -R 777 bootstrap/cache
	sudo chmod -R 777 storage

# Permissions alias
perm: permissions

# Generate a Laravel app key
key:
	docker-compose exec php php artisan key:generate --ansi

# Generate a Laravel storage symlink
storage:
	docker-compose exec php php artisan storage:link

# PHP composer autoload command
autoload:
	docker-compose exec php composer dump-autoload

npm:
	docker-compose exec php npm i && npm run build

#-----------------------------------------------------------
# Clearing
#-----------------------------------------------------------

# Shut down and remove all volumes
remove-volumes:
	docker-compose down --volumes

# Remove all existing networks (useful if network already exists with the same attributes)
prune-networks:
	docker network prune



# Install the environment
install: build env composer-install composer-update key storage permissions
<?php

namespace App\Console\Commands;

use App\Models\CryptoCurrency;
use App\Models\Currency;
use App\Models\CurrentCryptoCourse;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class UpdateCurrentCourse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'course:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $offset = 0;
        $limit = 50;
        $currencyStr = "";
        $cryproCurrencyStr = "";

        $currency = CryptoCurrency::select('name')->limit($limit)->get();
        foreach($currency as $row){
            $cryproCurrencyStr .= $row['name'].',';
        }
        $currency = Currency::select('name')->get();
        foreach($currency as $row){
            $currencyStr .= $row['name'].',';
        }
        $currencyStr = rtrim($currencyStr, ',');
        $cryproCurrencyStr = rtrim($cryproCurrencyStr, ',');
        $offset += $limit;

        $client = new Client(['base_uri' => 'https://min-api.cryptocompare.com']);
        $response = $client->request('GET', "/data/pricemulti?fsyms={$cryproCurrencyStr}&tsyms={$currencyStr}")->getBody()->getContents();
        $currencyStr = '';
        $cryproCurrencyStr = '';
        foreach(json_decode($response) as $key => $value){
            CurrentCryptoCourse::where('crypto_name', $key)->update([
                'price_usd' => $value->USD ?? null,
                'price_eur' => $value->EUR ?? null,
                'price_rub' => $value->RUB ?? null,
            ]);
        }
    }
}

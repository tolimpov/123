<?php

namespace App\Http\Controllers;

use App\Models\CryptoCurrency;
use App\Models\CurrentCryptoCourse;
use App\Models\Transactions;
use App\Models\Wallets;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WalletController extends Controller
{
    public function balanceReplenishment(Request $request){
        $user = auth()->id();
        $currency_id = CryptoCurrency::whereRaw("name ilike '{$request->currency}'");
        $data = Wallets::where('user_id', $user)->where('currency_id', $currency_id)->first();

        if($data){
            $value = $data->value + $request->value;
            Wallets::where('user_id', $user)->where('currency_id', $currency_id)->update([
                'value' => $value,
            ]);

            Transactions::create([
                'user_id' => $user,
                'currency_id_from' => $currency_id,
                'value_from' => $request->value,
                'transaction_type' => 1
            ]);
        }else{
            Wallets::create([
                'user_id' => $user,
                'currency_id' => $currency_id,
                'value' => $request->value,
            ]);

            Transactions::create([
                'user_id' => $user,
                'currency_id_from' => $currency_id,
                'value_from' => $request->value,
                'transaction_type' => 1
            ]);
        }

        return response('success', 200);
    }

    public function getUserWallet(Request $request){
        $user = auth()->id();
        if($request->has('type')){
            return DB::table('wallets')
                ->where('wallets.user_id', $user)
                ->where('type', $request->type)
                ->leftJoin('crypto_currency', 'wallets.currency_id', '=', 'crypto_currency.id')
                ->get();
        }else{
            return DB::table('wallets')
                ->where('wallets.user_id', $user)
                ->leftJoin('crypto_currency', 'wallets.currency_id', '=', 'crypto_currency.id')
                ->get();
        }
    }

    public function balanceWithdrawal(Request $request){
        $user = auth()->id();
        $currency_id = CryptoCurrency::whereRaw("name ilike '{$request->currency}'");
        $data = Wallets::where('user_id', $user)->where('currency_id', $currency_id)->first();

        if($data){
            $value = $data->value - $request->value;
            Wallets::where('user_id', $user)->where('currency_id', $currency_id)->update([
                'value' => $value,
            ]);

            Transactions::create([
                'user_id' => $user,
                'currency_id_from' => $currency_id,
                'value_from' => $request->value,
                'transaction_type' => 4
            ]);

            return response('success', 200);
        }
        return response('failed: undefined currency', 400);
    }
    // public function getUserTransactions()
    public function currencyExchangeToCrypto(Request $request){
        $user = auth()->id();
        $id_currency_to = CryptoCurrency::whereRaw("name ilike '{$request->to}'")->first();
        $id_currency_to = $id_currency_to->id;
        $currentCourse = CurrentCryptoCourse::where('id', $id_currency_to)->first();
        if($request->from == 'rub'){
            $value = DB::table('wallets')->select('value')->whereRaw("crypto_currency.name ilike '{$request->from}'")
            ->leftJoin('crypto_currency', 'wallets.currency_id', '=', 'crypto_currency.id')->first();
            if($value->value < $currentCourse->price_rub * $request->value){
                return response("not enough {$request->from}", 400);
            }else{
                Wallets::where('user_id', $user)->where('currency_id', 51)->update([
                    'value' => $value->value - $request->value * $currentCourse->price_rub * 0.95,
                ]);

                $data = Wallets::where('user_id', $user)->where('currency_id', $request->to)->first();

                if($data){
                    $value = $data->value + $request->value;
                    Wallets::where('user_id', $user)->where('currency_id', $id_currency_to)->update([
                        'value' => $value,
                    ]);
        
                    Transactions::create([
                        'user_id' => $user,
                        'currency_id_from' => 51,
                        'value_from' => $currentCourse->price_rub * $request->value * 0.95,
                        'value_to' => $request->value,
                        'currency_id_to' => $id_currency_to,
                        'transaction_type' => 3,
                    ]);
                }else{
                    Wallets::create([
                        'user_id' => $user,
                        'currency_id' => $id_currency_to,
                        'value' => $request->value,
                    ]);
        
                    Transactions::create([
                        'user_id' => $user,
                        'currency_id_from' => 51,
                        'value_from' => $currentCourse->price_rub * $request->value * 0.95,
                        'value_to' => $request->value,
                        'currency_id_to' => $id_currency_to,
                        'transaction_type' => 3,
                    ]);
                }
            };
            
        }else return response('operation unavailable', 400);
    }

    public function getTransactions(){
        $user = auth()->id();
        $transaction = Transactions::where('user_id', $user)->get();
        return $transaction;
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\CryptoCurrency;
use App\Models\Currency;
use App\Models\CurrentCryptoCourse;
use Illuminate\Http\Request;

class ExchangeRatesController extends Controller

{   
    public function getCurrency(Request $request){
        if($request->has('type')){
            return CryptoCurrency::where('type', $request->type)->get();
        }
        $result = CryptoCurrency::get();
        return $result;
    }

    public function getCurrentCourse(Request $request){
        $sqlSort = 'id ASC';
        $sortBy= '';
        if($request->has('sort')){
            $data = json_decode($request->sort, true);
            foreach($data as $row){
                $row = strtolower($row);
                if($row == 'asc' || $row == 'desc') $sort = $row;
                if($row == 'rub' || $row == 'usd' || $row == 'eur') $sortBy = 'price_'.$row;
                if($row == 'name') $sortBy = 'crypto_name';
            }
            if($sortBy !== 'price_') $sortBy = 'price_usd';
            $sqlSort = "{$sortBy} {$sort}";
        }

        if($request->has('limit')){ 
            $totalPage = ceil(CurrentCryptoCourse::count() / $request->limit) ?? 0;
            $currentPage = ceil($request->offset / $request->limit);
            $result = CurrentCryptoCourse::orderbyraw($sqlSort. " LIMIT {$request->limit} OFFSET {$request->offset}")->get();
        } else $result = CurrentCryptoCourse::orderbyraw($sqlSort)->get();
        
        $result = ['meta' => ['total_page' => $totalPage ?? null, 'current_page' => $currentPage ?? null], 'Data' => $result];

        return $result;
    }

    public function searchByName(Request $request){
        $result = CurrentCryptoCourse::whereRaw("crypto_name ilike '{$request->name}'")->get();
        return $result;
    }
}


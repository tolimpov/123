<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Wallets extends Model
{
    protected $table = 'wallets';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'currency_id',
        'type',
        'value',
    ];
}

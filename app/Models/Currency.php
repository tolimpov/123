<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currency';
    protected $primarykey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];
}

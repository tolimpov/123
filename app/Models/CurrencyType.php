<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyType extends Model
{
    protected $table = 'currency_type';
    protected $primarykey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];
}

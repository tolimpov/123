<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrentCryptoCourse extends Model
{
    protected $table = 'current_crypto_courses';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'crypto_name',
        'price_usd',
        'price_eur',
        'price_rub'
    ];
}

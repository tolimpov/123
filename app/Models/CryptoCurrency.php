<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CryptoCurrency extends Model
{
    protected $table = 'crypto_currency';
    protected $primarykey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'full_name',
        'type'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    protected $table = 'transaction';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'currency_id_from',
        'value_from',
        'currency_id_to',
        'value_to',
        'transaction_type'
    ];
}

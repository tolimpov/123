<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogOutController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\ExchangeRatesController;
use App\Http\Controllers\WalletController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('currency', [ExchangeRatesController::class, 'getCurrency']);
Route::post('current_course', [ExchangeRatesController::class, 'getCurrentCourse']);
Route::post('search', [ExchangeRatesController::class, 'searchByName']);

// Route::get('add', [WalletController::class, 'balanceReplenishment'])->middleware('api');

Route::group(['namespace' => 'Auth'], function(){
    Route::post('register', [RegisterController::class, 'register']);
    Route::post('login', [LoginController::class, 'login'])->name('login');
    Route::post('logout', [LogOutController::class], 'logout');
});

Route::middleware('auth:api')->group(function() {
    Route::get('replenishment', [WalletController::class, 'balanceReplenishment']);
    Route::get('wallets', [WalletController::class, 'getUserWallet']);
    Route::get('withdrawal', [WalletController::class, 'balanceWithdrawal']);
    Route::get('exchange', [WalletController::class, 'currencyExchangeToCrypto']);
    Route::get('transaction' , [WalletController::class, 'getTransactions']);
});
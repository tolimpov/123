<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction', function(Blueprint $table){
            $table->renameColumn('crypto_id', 'currency_id_from');
            $table->renameColumn('currency_id', 'currency_id_to');
            $table->renameColumn('value', 'value_from');
            $table->renameColumn('currency_value', 'value_to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('current_crypto_courses', function(Blueprint $table){
            $table->id();
            $table->string('crypto_name');
            $table->double('price_usd')->nullable();
            $table->double('price_eur')->nullable();
            $table->double('price_rub')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('current_crypto_courses');
    }
};
